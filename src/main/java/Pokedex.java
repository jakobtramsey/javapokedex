/**     Description: This is a Pokedex repl project that is based off the Boot.Dev golang project.
 *      Author: Jakob Ramsey
 *      Date: 4/29/2024
 *
 */

import java.util.*;


public class Pokedex {

    public static void main(String[] args) {
         HashMap<String, cliCommand> commands = callCommands();
        Scanner input = new Scanner(System.in);
        while(true){
            System.out.print("Pokedex > ");
            String enteredCommand = input.nextLine().toLowerCase();
            if(commands.containsKey(enteredCommand)){
                commands.get(enteredCommand).callbacks(commands);
            } else{
                System.out.println("Unkown command");
            }
        }
    }

    public static HashMap<String, cliCommand> callCommands(){
        HashMap<String, cliCommand> commandMap = new HashMap<>();
        commandMap.put("help", new cliCommand("help", "Displays a help message", "commandHelp"));
        commandMap.put("exit", new cliCommand("exit", "Exits the Pokedex", "commandExit"));
        return commandMap;
    }


}

class cliCommand {
public  String name;
public  String description;
public  String command;

public cliCommand(String name, String description, String command) {
    this.name = name;
    this.description = description;
    this.command = command;
}


public void callbacks(HashMap<String, cliCommand> commands){
    switch (this.name){
        case "help": callbackHelp(commands);
        break;
        case "exit": callbackExit();
        break;
    }
}

public void callbackExit(){
    System.exit(0);
}

public void callbackHelp(HashMap<String, cliCommand> commandMap){
    System.out.println("Welcome to Pokedex");
    System.out.println("Usage:");
    System.out.println();
    for (var cmd:commandMap.entrySet()){
        System.out.print(cmd.getValue().name);
        System.out.println(": " + cmd.getValue().description);
    }
    System.out.println();
}


}
