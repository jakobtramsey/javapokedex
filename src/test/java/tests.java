import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;


public class tests {


    @Test
    public void creationTest() {
        cliCommand command = new cliCommand("test", "this is a test command", "callbackTest");
        Assert.assertEquals("test", command.name);
        Assert.assertEquals("this is a test command", command.description);
        Assert.assertEquals("callbackTest", command.command);
    }



}
